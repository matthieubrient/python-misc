# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 11:17:27 2020

@author: matth
"""
#print element 2 by 2
Good="GsoAo+d"
print(Good[::2])

###edit and sort tuple
C_tuple=(-5, 1, -3)
C_tuple = sorted(C_tuple)
print(C_tuple)

###edit list
a_list = [1, 'hello', [1,2,3] and True]
print(a_list)

###combine list
A = [1, 'a']  
B = [2, 1, 'd']

list = A + B
print(list)

###add to a set
S = {'A','B','C'}
S.add('D')
print(S)

###find intersection between two sets
A={1,2,3,4,5}
B={1,3,9, 12}

C=A.intersection(B)
print(C)

###union two sets

S={'A','B','C'}

U={'A','Z','C'}

print(U.union(S))

###compare sum of list and set list
A = [1, 2, 2, 1]
B = set([1, 2, 2, 1])

print("the sum of A is:", sum(A))
print("the sum of A is:", sum(B))

# Check if subset

album_set1 = set(["Thriller", 'AC/DC', 'Back in Black'])
album_set2 = set([ "AC/DC", "Back in Black", "The Dark Side of the Moon"])

album_set3 = album_set1.union(album_set2)
print(album_set3)
print(set(album_set1).issubset(album_set3))    

# Define a function for multiple two numbers

def Mult(a, b):
    c = a * b
    return(c)
    print('This is not printed')
    
result = Mult(12,2)
print(result)


# Define a function for dividing two numbers

def div(a, b):
    return(a/b)


# Define a function for multiple two numbers

def Mult(a, b):
    c = a * b
    return(c)
    print('This is not printed')
    
result = Mult(12,2)
print(result)



# Edit and print dataframe 

import pandas as banana

df=banana.DataFrame({'a':[11,22,31],'b':[21,22,23]})

print(df.head())

print('The value of the second item in the first colonn is: ')  
print(df.iloc[1,0])


# Import the libraries

import time 
import sys
import numpy as np 

import matplotlib.pyplot as plt
%matplotlib inline  

# Plotting functions

def Plotvec1(u, z, v):
    
    ax = plt.axes()
    ax.arrow(0, 0, *u, head_width=0.05, color='r', head_length=0.1)
    plt.text(*(u + 0.1), 'u')
    
    ax.arrow(0, 0, *v, head_width=0.05, color='b', head_length=0.1)
    plt.text(*(v + 0.1), 'v')
    ax.arrow(0, 0, *z, head_width=0.05, head_length=0.1)
    plt.text(*(z + 0.1), 'z')
    plt.ylim(-2, 2)
    plt.xlim(-2, 2)

def Plotvec2(a,b):
    ax = plt.axes()
    ax.arrow(0, 0, *a, head_width=0.05, color ='r', head_length=0.1)
    plt.text(*(a + 0.1), 'a')
    ax.arrow(0, 0, *b, head_width=0.05, color ='b', head_length=0.1)
    plt.text(*(b + 0.1), 'b')
    plt.ylim(-2, 2)
    plt.xlim(-2, 2)
    
# Create a python list

a = ["0", 1, "two", "3", 4]

# Print each element

print("a[0]:", a[0])
print("a[1]:", a[1])
print("a[2]:", a[2])
print("a[3]:", a[3])
print("a[4]:", a[4])
